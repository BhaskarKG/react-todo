import React from "react";
import { render } from "react-dom";
import WebRoutes from "./web-routes/WebRoutes";
import "./src/scss/site.scss";

render(<WebRoutes />, document.getElementById("root"));
