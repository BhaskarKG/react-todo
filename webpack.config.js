const merge = require("webpack-merge");
const commonConfig = require("./configs/webpack/common");
const devConfig = require("./configs/webpack/dev");
const prodConfig = require("./configs/webpack/prod");

module.exports =
    process.env.WEBPACK_MODE === "development"
        ? merge(commonConfig, devConfig)
        : merge(commonConfig, prodConfig);
