import React from "react";
import { Link } from "react-router-dom";
import "./scss/welcome.scss";

const Welcome = () => {
    return (
        <div className="welcome">
            <h1>Welcome to React exercises</h1>
            <div className="react-features-contianer">
                <Link to="/todo" className="href-btn href-btn-default btn-lg">
                    React Without Hooks
                </Link>
                <Link
                    to="/todoWithHooks"
                    className="href-btn href-btn-default btn-lg"
                >
                    React With Hooks
                </Link>
            </div>
        </div>
    );
};

export default Welcome;
