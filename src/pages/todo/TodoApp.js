import React, { Component } from "react";
import uuidV4 from "uuid";
import moment from "moment";
import TodoForm from "./components/container/TodoForm";
import TodoSortTasks from "./components/container/TodoSortTasks";
import TodoList from "./components/presentational/TodoList";
import {
    sortByDatePropInObjectASC,
    sortByDatePropInObjectDESC,
    sortByStringPropInObjectASC,
    sortByStringPropInObjectDESC,
    sortByBooleanPropInObjectASC,
    sortByBooleanPropInObjectDESC
} from "../../utilities/sortlib";
import "./scss/todo.scss";

export default class TodoApp extends Component {
    constructor(props) {
        super(props);
        this.state = {
            tasks: []
        };
        this.handleAddTaskRequest = this.handleAddTaskRequest.bind(this);
        this.handleRemoveTaskRequest = this.handleRemoveTaskRequest.bind(this);
        this.handleCompleteTaskRequest = this.handleCompleteTaskRequest.bind(
            this
        );
        this.handleSortTasksRequest = this.handleSortTasksRequest.bind(this);
    }

    handleAddTaskRequest(v) {
        this.setState(prevState => ({
            ...prevState,
            tasks: [
                ...prevState.tasks,
                {
                    id: uuidV4(),
                    title: v,
                    completed: false,
                    createdDate: moment().format("YYYY-MM-DD HH:mm:ss"),
                    updatedDate: moment().format("YYYY-MM-DD HH:mm:ss")
                }
            ]
        }));
    }

    handleRemoveTaskRequest(id) {
        let tasks = [];
        const taskId = this.state.tasks.findIndex(
            taskObject => taskObject.id === id
        );
        if (taskId === -1) return;
        tasks = [...this.state.tasks];
        tasks.splice(taskId, 1);
        this.setState(prevState => ({ ...prevState, tasks }));
    }

    handleCompleteTaskRequest(id) {
        let tasks = [];
        tasks = [...this.state.tasks];
        for (let i = 0; i < tasks.length; i++) {
            if (tasks[i].id === id) {
                tasks[i].completed = true;
                break;
            }
        }
        this.setState(prevState => ({ ...prevState, tasks }));
    }

    handleSortTasksRequest(sortPattern) {
        let tasks = [];

        const sortBy = sortPattern.substr(0, sortPattern.lastIndexOf("-"));
        const sortType = sortPattern.substr(
            sortPattern.indexOf("-") + 1,
            sortPattern.length
        );
        tasks = [...this.state.tasks];
        if (sortType.toLowerCase() === "asc") {
            switch (sortBy) {
                case "cdate":
                    tasks.sort(sortByDatePropInObjectASC("createdDate"));
                    break;
                case "title":
                    tasks.sort(sortByStringPropInObjectASC("createdDate"));
                    break;
                case "completed":
                    tasks.sort(sortByBooleanPropInObjectDESC("completed"));
                    break;
                default:
                    tasks.sort();
            }
        } else if (sortType.toLowerCase() === "desc") {
            switch (sortBy) {
                case "cdate":
                    tasks.sort(sortByDatePropInObjectDESC("createdDate"));
                    break;
                case "title":
                    tasks.sort(sortByStringPropInObjectDESC("createdDate"));
                    break;
                case "completed":
                    tasks.sort(sortByBooleanPropInObjectASC("completed"));
                    break;
                default:
                    tasks.sort();
            }
        }
        this.setState(prevState => ({ ...prevState, tasks }));
    }

    render() {
        return (
            <div className="todoapp">
                <TodoForm handleAddTaskRequest={this.handleAddTaskRequest} />
                {this.state.tasks.length > 1 && (
                    <TodoSortTasks
                        handleSortTasksRequest={this.handleSortTasksRequest}
                    />
                )}
                <TodoList
                    tasks={this.state.tasks}
                    handleRemoveTaskRequest={this.handleRemoveTaskRequest}
                    handleCompleteTaskRequest={this.handleCompleteTaskRequest}
                />
            </div>
        );
    }
}
