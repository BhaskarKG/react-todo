import React from "react";
import PropTypes from "prop-types";

const TodoTaskCard = props => {
    const { task, handleRemoveTaskRequest, handleCompleteTaskRequest } = props;
    return (
        <div
            className={`task-card ${
                task.completed ? "task-completed" : "task-open"
            }`}
        >
            <div className="task-title">
                <h3>{task.title}</h3>
            </div>
            <div className="task-created-date">
                <h5>Created At: {task.createdDate}</h5>
            </div>
            <div className="task-actions">
                {task.completed === false && (
                    <button
                        type="button"
                        className="btn btn-success btn-md"
                        onClick={() => handleCompleteTaskRequest(task.id)}
                    >
                        COMPLETE
                    </button>
                )}
                <button
                    type="button"
                    className="btn btn-danger btn-md"
                    onClick={() => handleRemoveTaskRequest(task.id)}
                >
                    DELETE
                </button>
            </div>
        </div>
    );
};

TodoTaskCard.propTypes = {
    task: PropTypes.object.isRequired,
    handleCompleteTaskRequest: PropTypes.func.isRequired,
    handleRemoveTaskRequest: PropTypes.func.isRequired
};

export default TodoTaskCard;
