import React from "react";

const TodoMessage = () => {
    return (
        <div className="todo-message">
            <h3>No Tasks Found</h3>
        </div>
    );
};

export default TodoMessage;
