import React from "react";
import PropTypes from "prop-types";
import TodoTaskCard from "./TodoTaskCard";
import TodoMessage from "./TodoMessage";

const TodoList = props => {
    const { tasks, handleRemoveTaskRequest, handleCompleteTaskRequest } = props;
    function renderTasksList() {
        return tasks.map(task => (
            <TodoTaskCard
                key={task.id}
                task={task}
                handleRemoveTaskRequest={handleRemoveTaskRequest}
                handleCompleteTaskRequest={handleCompleteTaskRequest}
            />
        ));
    }
    return (
        <React.Fragment>
            <div className="todo-list">
                {tasks.length === 0 && <TodoMessage />}
                {tasks.length > 0 && renderTasksList()}
            </div>
        </React.Fragment>
    );
};

TodoList.propTypes = {
    tasks: PropTypes.array.isRequired,
    handleRemoveTaskRequest: PropTypes.func.isRequired,
    handleCompleteTaskRequest: PropTypes.func.isRequired
};

export default TodoList;
