import React, { Component } from "react";
import PropTypes from "prop-types";

class TodoSortTasks extends Component {
    constructor(props) {
        super(props);
        this.state = {
            sortBy: ""
        };
        this.captureSortRequest = this.captureSortRequest.bind(this);
    }

    captureSortRequest(event) {
        const v = event.target.value;
        if (!v) return;
        this.setState(prevState => ({ ...prevState, sortBy: v }));
        this.props.handleSortTasksRequest(v);
    }

    render() {
        return (
            <div className="todo-sort-container">
                Sort By: &nbsp;
                <select
                    value={this.state.sortBy}
                    onChange={this.captureSortRequest}
                >
                    <option value="">Select sorting type</option>
                    <option value="cdate-asc">Created Date (ASC)</option>
                    <option value="cdate-desc">Created Date (DESC)</option>
                    <option value="title-asc">Task Title (ASC)</option>
                    <option value="title-desc">Task Title (DESC)</option>
                    <option value="completed-asc">
                        Show Completed Tasks Top
                    </option>
                    <option value="completed-desc">Show Open Tasks Top</option>
                </select>
            </div>
        );
    }
}

TodoSortTasks.propTypes = {
    handleSortTasksRequest: PropTypes.func.isRequired
};

export default TodoSortTasks;
