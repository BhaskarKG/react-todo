import React, { Component } from "react";
import PropTypes from "prop-types";

class TodoForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            taskTextFieldValue: ""
        };
        this.captureUserTaskField = this.captureUserTaskField.bind(this);
        this.addTask = this.addTask.bind(this);
    }

    captureUserTaskField(event) {
        const v = event.target.value;
        this.setState(prevState => ({
            ...prevState,
            taskTextFieldValue: v
        }));
    }

    addTask(event) {
        event.preventDefault();
        const newTask = this.state.taskTextFieldValue.trim();
        if (!newTask) return;
        this.props.handleAddTaskRequest(newTask);
        this.setState({ taskTextFieldValue: "" });
    }

    render() {
        return (
            <div className="todo-form">
                <input
                    type="text"
                    className="taskTextField"
                    id="taskTextField"
                    placeholder="Enter new task"
                    onChange={this.captureUserTaskField}
                    value={this.state.taskTextFieldValue}
                />
                <button
                    type="button"
                    id="addTaskButton"
                    className="btn btn-primary btn-md addTaskButton"
                    onClick={this.addTask}
                >
                    ADD TASK
                </button>
            </div>
        );
    }
}

TodoForm.propTypes = {
    handleAddTaskRequest: PropTypes.func
};

export default TodoForm;
