import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faAward } from "@fortawesome/free-solid-svg-icons/faAward";

const TodoWithHooks = () => {
    return (
        <div className="todo-with-hooks">
            <FontAwesomeIcon icon={faAward} />
        </div>
    );
};

export default TodoWithHooks;
