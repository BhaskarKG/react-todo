export function sortByNumberASC() {
    return (a, b) => a - b;
}

export function sortByNumberDESC() {
    return (a, b) => b - a;
}

export function sortByStringASC() {
    return (a, b) => {
        const x = a.toLowerCase();
        const y = b.toLowerCase();
        if (x < y) {
            return -1;
        } else if (x > y) {
            return 1;
        }
        return 0;
    };
}

export function sortByStringDESC() {
    return (a, b) => {
        const x = a.toLowerCase();
        const y = b.toLowerCase();
        if (y < x) {
            return -1;
        } else if (y > x) {
            return 1;
        }
        return 0;
    };
}

export function sortByStringPropInObjectASC(prop) {
    return (a, b) => {
        const x = a[prop].toLowerCase();
        const y = b[prop].toLowerCase();
        if (x < y) {
            return -1;
        } else if (x > y) {
            return 1;
        }
        return 0;
    };
}

export function sortByStringPropInObjectDESC(prop) {
    return (a, b) => {
        const x = a[prop].toLowerCase();
        const y = b[prop].toLowerCase();
        if (y < x) {
            return -1;
        } else if (y > x) {
            return 1;
        }
        return 0;
    };
}

export function sortByNumberPropInObjectASC(prop) {
    return (a, b) => a[prop] - b[prop];
}

export function sortByNumberPropInObjectDESC(prop) {
    return (a, b) => b[prop] - a[prop];
}

export function sortByDatePropInObjectASC(prop) {
    return (a, b) => new Date(a[prop]) - new Date(b[prop]);
}

export function sortByDatePropInObjectDESC(prop) {
    return (a, b) => new Date(b[prop]) - new Date(a[prop]);
}

export function sortByBooleanPropInObjectASC(prop) {
    return (a, b) => a[prop] - b[prop];
}

export function sortByBooleanPropInObjectDESC(prop) {
    return (a, b) => b[prop] - a[prop];
}
