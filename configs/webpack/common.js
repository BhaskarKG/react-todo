const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");

module.exports = {
    entry: path.resolve(__dirname, "..", "..", "App.js"),
    plugins: [
        new HtmlWebpackPlugin({
            template: path.resolve(__dirname, "..", "..", "src", "index.html"),
            filname: path.resolve(__dirname, "..", "..", "dist", "index.html")
        }),
        new CopyWebpackPlugin([
            {
                from: path.resolve(__dirname, "..", "..", "src", "images"),
                to: path.resolve(__dirname, "..", "..", "dist", "images")
            }
        ])
    ]
};
