const path = require("path");

module.exports = {
    mode: "development",
    output: {
        path: path.resolve(__dirname, "..", "..", "dist"),
        filename: "bundle.js"
    },
    devtool: "inline-source-map",
    module: {
        rules: [
            {
                test: /\.js$/i,
                exclude: /node_modules/i,
                use: "babel-loader"
            },
            {
                test: /\.(ttf|eot|woff|woff2)$/i,
                exclude: /node_modules/i,
                use: [
                    {
                        loader: "url-loader",
                        options: {
                            limit: 8000,
                            name: "fonts/[name].[ext]"
                        }
                    }
                ]
            },
            {
                test: /\.(jpg|jpeg|png|gif|svg)$/i,
                exclude: /node_modules/i,
                use: [
                    {
                        loader: "file-loader",
                        options: {
                            name: "[name].[hash].[ext]",
                            outputPath: "images"
                        }
                    },
                    {
                        loader: "url-loader",
                        options: {
                            limit: 10000,
                            name: "images/[name].[hash].[ext]"
                        }
                    }
                ]
            },
            {
                test: /\.(sa|sc|c)ss$/i,
                exclude: /node_modules/i,
                use: ["style-loader", "css-loader", "sass-loader"]
            }
        ]
    },
    devServer: {
        contentBase: path.resolve(__dirname, "..", "..", "dist"),
        hot: true,
        open: true,
        port: 9496,
        publicPath: "/",
        historyApiFallback: true
    }
};
