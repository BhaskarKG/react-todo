import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Header from "../src/common/components/presentational/Header";
import TodoApp from "../src/pages/todo/TodoApp";
import TodoWithHooks from "../src/pages/todoWithHooks/TodoWithHooks";
import Welcome from "../src/pages/welcome/Welcome";

const WebRoutes = () => {
    return (
        <React.Fragment>
            <Header />
            <div className="body-content">
                <Router>
                    <Switch>
                        <Route exact path="/" component={Welcome} />
                        <Route path="/todo" component={TodoApp} />
                        <Route
                            path="/todoWithHooks"
                            component={TodoWithHooks}
                        />
                    </Switch>
                </Router>
            </div>
        </React.Fragment>
    );
};

export default WebRoutes;
